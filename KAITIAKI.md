# kaitiaki subgroup spec

A specification for implementing kaitiaki-only subgroups in scuttlebutt.

The fundamentals of this spec are the same as private subgroups with differences:

* **members** of the kaitiaki-only subgroup are:
    * the creator of the **group**
    * people added with `group/add-member` messages
* each **group** will have at most one kaitiaki-only subgroup attached to it
    * it can have any number of _other_ (non-kaitiaki) subgroups

## How kaitiaki-only subgroups work

### Āhau use case

1. a  **group** is made with its own `groupKey`
2. a **kaitiaki-only subgroup** is auto-made with its own `groupKey` and `poBoxId`
3. the **kaitiaki** are the members of the kaitiaki-only subgroup
    * the addition of a member to this subgroup should be reflected by updating the `authors` field of the group's community profile 
4. there is a **link** connecting the group + subgroup to help identify it:
    ```js
    {
      type: 'link/group-group/subgroup',
      parent: parentGroupId,
      child: childGroupId,

      admin: true,            // << Important!
       
      tangles: {
        link: { root: null, previous: null },
        group: { root: parentGroupRoot, previous: [...] },
      },

      recps: [parentGroupId]  // << means visible to group members
    }
    ```
5. the **poBoxId** of the kaitiaki only group is advertised:
    - on the `group/po-box` message sent to members of kaitiaki subgroup
    - on the parent groups public community profile (so that people can apply to join the parent group by DM'ing the kaitiaki)


## Messages
```mermaid
graph TB

community0["profile/community (0)"]
link0A([link/group-profile])
poBoxIdVal0[poBoxId]

subgraph group
  groupId1((group ID))
  groupId1((group ID))
  init1[group/init]
  
  community1["profile/community (1)"]
  link1([link/group-profile])
  poBoxIdVal1[poBoxId]
  
  linkGroupSubgroup([link/group-group/subgroup])
  
  admin[admin: true]
  
  subgraph kaitiaki sub-group
    groupId2((group ID))
    init2[group/init]
    poBoxId2[group/po-box]

    poBoxIdVal2[poBoxId]
  end
end

groupId1-->link0A-->community0
community0 -.- poBoxIdVal0

init1-.->groupId1
groupId1-->link1-->community1
groupId1-->linkGroupSubgroup-->groupId2
linkGroupSubgroup-.-admin
community1-.-poBoxIdVal1

init2-->poBoxId2
init2-.->groupId2
poBoxId2 -.- poBoxIdVal2


classDef default fill:#990099, stroke:purple, stroke-width 1, color: white;
classDef cluster fill:#ff00ff22, stroke:purple, border-radius: 20px;

classDef off-chain fill:gainsboro, stroke:black, stroke-dasharray: 2;
class groupId1,groupId2 off-chain

classDef subgroup-msg fill:#0000bb, stroke:#0000bb, stroke-width 1, color: white;
class init2,poBoxId2,community2,link2A,communitySubgroup,linkSubgroupProfile subgroup-msg

classDef public-msg fill:white, stroke:purple, stroke-width: 1, color: black;
class community0,link0A public-msg;

classDef meta-data fill:#0000bb22, stroke:black, stroke-width: 1;
class poBoxIdVal0,poBoxIdVal1,poBoxIdVal2,admin,parentGroupId1 meta-data
```

### Who are the kaitiaki?

The source of truth of who the kaitiaki are is the member of the kaitiaki subgroup.

When that membership changes, you SHOULD update:
- public profile/community (0) `authors`  (TODO: check we want this)
- group profile/community (1) `authors`


### How do you contact the kaitiaki?

The kaitiaki subgroup advertises it's `poBoxId` (which it created with `group/po-box` message) on:
- public profile/community (0) `poBoxId`
- group profile/community (1) `poBoxId`


### Notes

1. we currently don't have any profile/community for the group (in sub/parent group):
    - in the case of admin subgroup, it's VERY closely related to the parent group (not the same for general subgroups)
    - we don't *think* it's needed yet


